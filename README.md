# Realestate_price_visualization

## 背景
- 不動産価格の高騰に興味がある．
- 東京→神奈川→埼玉→千葉，と時計回りに高騰しているらしい．
- 土地勘のある千葉に注目して価格変化を調べる

## やりたいこと
- 各年度の価格変化を可視化したい（バブル，色など？）
- どの地区が多く売れている・動いていないなども可視化したい．

## やったこと
1. 不動産取引価格情報取得APIによる価格情報の取得
    - 全国市町村の不動産取引情報をとれる．
    - <https://www.land.mlit.go.jp/webland/api.html>
    - output.csvとして出力
    <br>

2. 不動産価格情報output.csvの前処理
    - ここがすごく面倒くさい．自動化が難しいと感じた．
        - わりと欠損値が多い
        - "㎡"などの特殊文字が使われている
        - 数値入力欄に文字列が入っている
        <br>

3. 不動産価格情報output.csv内の住所と緯度経度の紐づけ
    - 地図上に各物件の位置を示すには緯度経度が必要
    - output.csvには町名まで記載されているので，町名の緯度経度をoutput.csvに追加したい．
    - 緯度経度と町名の関係は→で取得  [Geolinia](https://github.com/geolonia/japanese-addresses)
    - Adress_chiba-city.csvとして出力
    <br>

4. 住所と緯度経度情報Adress_chiba-city.csvの前処理
    - ○○丁目までの情報はoutput.csvにないので，Adress_chiba-city.csvから削除
    - 安藤さんのLinuxコマンドで実施
    <br>

5. 不動産価格情報output.csvと住所と緯度経度情報Adress_chiba-city.csvのマージ
    - output.csvの各町名に対応した緯度経度をAdress_chiba-city.csvから取得して，緯度列・経度列をoutput.csvに加える．
    - 手動でoutput.csvを.txtで保存．ANSIを文字コードとして選ぶ．
    - 改めてエクセルからデータインポートで上記txtを読んで保存する作業が必要．
    - <font color="Red">csvの文字コードの処理が大変だった．</font>
    - 手動作業が必要なのが課題．
    <br>
6. 地図上へのプロット
    - foliumライブラリの活用  [foliumの基本的な使い方とオープンデータ活用](https://qiita.com/Kumanuron-1910/items/12ce7aa02922927de2f4) 
    
